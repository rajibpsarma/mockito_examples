package org.bitbucket.rajibpsarma.ex1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Any;

public class DomainControllerTest {
	@Test
	// Test success condition
	public void createDomainWithNameTest1() {
		try {
			DomainService service = Mockito.mock(DomainService.class);
			Mockito.doNothing().when(service).createDomain(Mockito.anyString());
			
			DomainController controller = new DomainController();
			controller.setDomainService(service);
			
			boolean retVal = controller.createDomain("abc");
			System.out.println(retVal);
			assertEquals(true, retVal);
		} catch(Exception ex) {
			fail("Should not throw exception");
		}
	}
	
	// Test exception condition
	@Test
	public void createDomainWithNameTest2() {
		try {
			DomainService service = Mockito.mock(DomainService.class);
			Mockito.doThrow(new Exception()).when(service).createDomain(Mockito.anyString());
			
			DomainController controller = new DomainController();
			controller.setDomainService(service);
			
			boolean retVal = controller.createDomain("abc");
			assertEquals(false, retVal);
		} catch(Exception ex) {
			fail("Should not throw exception");
		}
	}
	
	@Test
	// Test success condition
	public void createDomainWithDomainTest1() {
		try {
			DomainService service = Mockito.mock(DomainService.class);
			Mockito.doNothing().when(service).createDomain(Mockito.any(Domain.class));
			
			DomainController controller = new DomainController();
			controller.setDomainService(service);
			
			boolean retVal = controller.createDomain(new Domain());
			System.out.println(retVal);
			assertEquals(true, retVal);
		} catch(Exception ex) {
			fail("Should not throw exception");
		}
	}
	
	// Test exception condition
	@Test
	public void createDomainWithDomainTest2() {
		try {
			DomainService service = Mockito.mock(DomainService.class);
			Mockito.doThrow(new Exception()).when(service).createDomain(Mockito.any(Domain.class));
			
			DomainController controller = new DomainController();
			controller.setDomainService(service);
			
			boolean retVal = controller.createDomain(new Domain());
			assertEquals(false, retVal);
		} catch(Exception ex) {
			fail("Should not throw exception");
		}
	}
	
	@Test
	// Test success condition
	public void getDomainTest1() {
		try {
			DomainService service = Mockito.mock(DomainService.class);
			Mockito.doReturn(new Domain()).when(service).getDomain(Mockito.anyString());
			
			DomainController controller = new DomainController();
			controller.setDomainService(service);
			
			Domain domain = controller.getDomain("abc");
			assertNotNull(domain);
		} catch(Exception ex) {
			fail("Should not throw exception");
		}
	}	
	
	@Test
	// Test exception condition
	public void getDomainTest2() {
		try {
			DomainService service = Mockito.mock(DomainService.class);
			//Mockito.doThrow(new DomainNotFoundException()).when(service).getDomain(Mockito.anyString());
			
			Mockito.when(service.getDomain(Mockito.anyString())).thenThrow(new DomainNotFoundException());
			
			DomainController controller = new DomainController();
			controller.setDomainService(service);
			
			Domain domain = controller.getDomain("abc");
			assertNull(domain);
		} catch(Exception ex) {
			fail("Should not throw exception");
		}
	}
}
