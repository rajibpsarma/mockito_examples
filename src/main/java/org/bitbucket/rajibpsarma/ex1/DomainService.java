package org.bitbucket.rajibpsarma.ex1;

public class DomainService {
	public void createDomain(String domainName) throws Exception {
		if(domainName == null) {
			throw new Exception();
		}
	}
	
	public void createDomain(Domain domain) throws Exception {
		if(domain == null) {
			throw new Exception();
		}
	}
	
	public Domain getDomain(String domainName) throws DomainNotFoundException {
		if(domainName == null) {
			throw new DomainNotFoundException();
		}
		return new Domain();
	}
}
