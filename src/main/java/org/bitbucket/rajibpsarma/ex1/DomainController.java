package org.bitbucket.rajibpsarma.ex1;

public class DomainController {
	private DomainService service;
	public void setDomainService(DomainService service) {
		this.service = service;
	}
	
	public boolean createDomain(String domainName) {
		boolean retVal = false;
		try {
			service.createDomain(domainName);
			retVal = true;
		} catch(Exception e) {}
		return retVal;
	}
	
	public boolean createDomain(Domain domain) {
		boolean retVal = false;
		try {
			service.createDomain(domain);
			retVal = true;
		} catch(Exception e) {}
		return retVal;
	}
	
	public Domain getDomain(String domainName) throws DomainNotFoundException {
		Domain domain = null;
		try {
			domain = service.getDomain(domainName);
		} catch(Exception e) {}
		return domain;
	}
}
